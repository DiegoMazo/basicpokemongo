﻿using UnityEngine;
using Globals;
using Globals.Auth;
using Firebase.Auth;
using Firebase;
using System.Collections;

public class AuthManager : MonoBehaviour
{
    private static FirebaseAuth auth;
    private static FirebaseUser user;
    private static DependencyStatus dependencyStatus;

    public static IEnumerator Login(string email, string password)
    {
        var LoginTask = auth.SignInWithEmailAndPasswordAsync(email, password);
        yield return new WaitUntil( () => LoginTask.IsCompleted);
        if (LoginTask.Exception != null)
        {
            Messenger.ShowMsg(string.Format(AuthMsg.FailedToLogin, LoginTask.Exception));
            FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;
            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

            switch (errorCode)
            {
                case AuthError.InvalidEmail:
                    Messenger.ShowMsg(AuthMsg.InvalidEmail);
                    break;
                case AuthError.MissingEmail:
                    Messenger.ShowMsg(AuthMsg.MissingEmail);
                    break;
                case AuthError.MissingPassword:
                    Messenger.ShowMsg(AuthMsg.MissingPassword);
                    break;
                case AuthError.WrongPassword:
                    Messenger.ShowMsg(AuthMsg.WrongPassword);
                    break;
                case AuthError.UserNotFound:
                    Messenger.ShowMsg(AuthMsg.UserNotFound);
                    break;
            }
        }
        else
        {
            user = LoginTask.Result;
            Messenger.ShowMsg(string.Format(AuthMsg.UserSignedInSuccessfully, user.DisplayName, user.Email));
            Messenger.Button.onClick.AddListener(LoadChasePokemonScene);
        }
    }
    public static IEnumerator Register(string username, string email, string password, string verificationPassword)
    {
        if (string.IsNullOrEmpty(username))
            Messenger.ShowMsg(AuthMsg.EmptyUserName);
        else if (!password.Equals(verificationPassword))
            Messenger.ShowMsg(AuthMsg.PasswordDoesNotMatch);
        else
        {
            var RegisterTask = auth.CreateUserWithEmailAndPasswordAsync(email, password);
            yield return new WaitUntil(() => RegisterTask.IsCompleted);

            if (RegisterTask.Exception != null)
            {
                Messenger.ShowMsg( string.Format(AuthMsg.FailedToRegister, RegisterTask.Exception));
                FirebaseException firebaseEx = RegisterTask.Exception.GetBaseException() as FirebaseException;
                
                AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

                switch (errorCode)
                {
                    case AuthError.InvalidEmail:
                        Messenger.ShowMsg(AuthMsg.InvalidEmail);
                        break;
                    case AuthError.MissingEmail:
                        Messenger.ShowMsg(AuthMsg.MissingEmail);
                        break;
                    case AuthError.MissingPassword:
                        Messenger.ShowMsg(AuthMsg.MissingPassword);
                        break;
                    case AuthError.WeakPassword:
                        Messenger.ShowMsg(AuthMsg.WeakPassword);
                        break;
                    case AuthError.EmailAlreadyInUse:
                        Messenger.ShowMsg(AuthMsg.EmailAlreadyInUse);
                        break;
                }
            }
            else
            {
                user = RegisterTask.Result;

                if (user != null)
                {
                    UserProfile profile = new UserProfile { DisplayName = username };
                    var ProfileTask = user.UpdateUserProfileAsync(profile);

                    yield return new WaitUntil(() => ProfileTask.IsCompleted);

                    if (ProfileTask.Exception != null)
                    {
                        Messenger.ShowMsg(string.Format(AuthMsg.FailedToRegister, ProfileTask.Exception));
                        FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
                        Messenger.ShowMsg(AuthMsg.UsernameSetFailed);
                    }
                    else 
                    {
                        Messenger.ShowMsg(AuthMsg.RegisterSuccessfully);
                        Messenger.Button.onClick.AddListener(ReturnToLogin);
                    }
                       
                }
            }
        }
    }

    private static void LoadChasePokemonScene() 
    {
        Messenger.Button.onClick.RemoveListener(LoadChasePokemonScene);
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }
    private static void ReturnToLogin() 
    {
        ScreensManager.ShowScreen(ScreenName.LoginScreen);
        Messenger.Button.onClick.RemoveListener(ReturnToLogin);
    }

    private void Start()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                auth = FirebaseAuth.DefaultInstance;
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }
}
