﻿using System.Collections;
using UnityEngine;
using Globals;
public class PokemonSpawner : MonoBehaviour
{
    private const float MIN_WAIT_TIME = 5;
    private const float MAX_WAIT_TIME = 7;
    public static PokemonSpawner Instance { get; private set; }
    [SerializeField] private Transform spawnPos;
    private Pokemon spawnedPokemon;
    private bool hasSpawned = false;
    public static Pokemon SpawnedPokemon => Instance.spawnedPokemon;
    public static int LastID { get; private set; }

    public IEnumerator SpawnPokemon()
    {
        if (hasSpawned) yield break;


        yield return new WaitForSeconds(Random.Range(MIN_WAIT_TIME, MAX_WAIT_TIME));
        spawnedPokemon = (Pokemon)PokemonPool.Instance.GetPooledObject();
  
        if (spawnedPokemon)
        {
            InGameUI_Manager.Instance.SetInGameText(InGameMsgs.APokemonHasAppeared);
            LastID = spawnedPokemon.ID;
            spawnedPokemon.ShowAt(spawnPos.position, spawnPos.rotation);
            GlobalEvents.PokemonDetected();
        }
        else
        {
            InGameUI_Manager.Instance.SetInGameText(InGameMsgs.AllPokemonCaptured);
            GlobalEvents.GameEnded();
        }
    }


    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
        GlobalEvents.Instance.OnCatchSuccess.AddListener
        (
            () =>
            {
                PokemonPool.Instance.RemovePokemon(SpawnedPokemon);
                StartCoroutine(SpawnPokemon());
            }
        );

        GlobalEvents.Instance.OnPokemonScape.AddListener
       (
           () =>
           {
               PokemonPool.Instance.RemovePokemon(SpawnedPokemon);
               StartCoroutine(SpawnPokemon());
           }
       );
    }

    private void Start()
    {
        InGameUI_Manager.Instance.SetInGameText(InGameMsgs.Lookaround);
        StartCoroutine(SpawnPokemon());
    }

}
