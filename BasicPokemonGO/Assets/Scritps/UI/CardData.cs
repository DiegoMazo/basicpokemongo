﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardData : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI idName;
    [SerializeField] private TextMeshProUGUI weight;
    [SerializeField] private TextMeshProUGUI height;
    [SerializeField] private Image img;
    public void SetData(PokemonGeneralData data) 
    {
        idName.text = string.Concat(data.id, ". ", data.name);
        height.text = string.Concat("height:\n ", data.height);
        weight.text = string.Concat("weight:\n ", data.weight);
    }
    public void SetImage(Texture2D texture)
    {
        img.sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
}
