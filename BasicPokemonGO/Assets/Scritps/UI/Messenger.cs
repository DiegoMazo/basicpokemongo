﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Messenger : MonoBehaviour
{
    public static Messenger Instance { get; private set; }
    [SerializeField] private GameObject renderGO;
    [SerializeField] private TextMeshProUGUI msgContent;
    [SerializeField] private Button button;

    public static Button Button => Instance.button;
    public static void ShowMsg(string msg = "") 
    {
        Instance.renderGO.SetActive(true);
        Instance.msgContent.text = msg;
    }

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        button.onClick.AddListener(()=>renderGO.SetActive(false));
    }
}
