﻿using UnityEngine;


public class Pokedex : MonoBehaviour
{

    private const ushort INDEX_MODIFIER = 10;
    private const ushort MIN_INDEX = 10, MAX_INDEX = 800;

    [SerializeField] private CardData[] cards;
    private ushort firstIndex, lastindex = 10;



    private ushort ClampIndex(ushort lastindex)
    {
        return (ushort)Mathf.Clamp(lastindex, MIN_INDEX, MAX_INDEX);
    }



    private void ShowData(ushort from, ushort to)
    {
        ushort counter = ushort.MinValue;
        for (int i = from; i < to; i++)
        {
            GetPokemonData.GetAllData(i, cards[counter]);
            counter++;
        }
    }

    public void PrintCards(bool increse)
    {

        lastindex = (ushort)(increse == true ? lastindex + INDEX_MODIFIER : lastindex - INDEX_MODIFIER);
        lastindex = ClampIndex(lastindex);
        firstIndex = (ushort)(lastindex - INDEX_MODIFIER);


        ShowData(firstIndex, lastindex);
    }
    private void Start
        ()
    {
        ShowData(ushort.MinValue, MIN_INDEX);
    }
}
