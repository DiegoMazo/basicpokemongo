﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class CreateAccountButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private TextMeshProUGUI text;

    private void OnEnable()
    {
        if (!text)
            text = GetComponent<TextMeshProUGUI>();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        text.color = Color.white;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        text.color = Color.blue;
       
    }
}
