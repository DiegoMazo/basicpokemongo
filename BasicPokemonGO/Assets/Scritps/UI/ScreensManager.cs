﻿using UnityEngine;

public class ScreensManager : MonoBehaviour
{
    public static ScreensManager Instance { get; private set; }
    [SerializeField] private GameObject[] screens;
    private static GameObject currentScreen;
    public static void ShowScreen(string name) 
    {
        if (currentScreen) 
        {
            if (currentScreen.name.Equals(name))
                return;

            currentScreen.SetActive(false);
        }

        for (int i = 0; i < Instance.screens.Length; i++)
        {
            if (Instance.screens[i].name.Equals(name))
            {
                currentScreen = Instance.screens[i];
                currentScreen.SetActive(true);
                return;
            }
        }
    }

    private void Awake()
    {
        if (!Instance) 
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        currentScreen = screens[ushort.MinValue];
    }
}
