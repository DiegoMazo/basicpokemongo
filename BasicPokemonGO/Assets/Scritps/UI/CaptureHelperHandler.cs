﻿using System.Collections;
using UnityEngine;

public class CaptureHelperHandler : MonoBehaviour
{
    private const ushort Z_POSITION = 1;
    private const float SCALING_FACTOR = 0.05f;


    [SerializeField] private Transform trapCircle;
    [SerializeField] private Transform scalingCircle;

    private void ToggleCircles(bool value)
    {
        trapCircle.gameObject.SetActive(value);
        scalingCircle.gameObject.SetActive(value);
    }
    public void SetPos(Vector3 position)
    {
        Vector3 helperPos = new Vector3(position.x, position.y, Z_POSITION);
        trapCircle.position = scalingCircle.position = helperPos;
    }

    public float GetCurrentScale => scalingCircle.localScale.x;

    private IEnumerator ScaleCircle()
    {
        ToggleCircles(true);
        do
        {
            scalingCircle.localScale -= Vector3.one * SCALING_FACTOR * Time.deltaTime;

            if (scalingCircle.localScale.x <= ushort.MinValue)
                scalingCircle.localScale = trapCircle.localScale;

            yield return null;
        } while (trapCircle.gameObject.activeSelf);
    }
    public void ShowIndicator() => StartCoroutine(ScaleCircle());

    public void HideIndicator()
    {
        StopAllCoroutines();
        scalingCircle.localScale = trapCircle.localScale;
        ToggleCircles(false);
    }

    private void Start()
    {
        ToggleCircles(false);
    }

}
