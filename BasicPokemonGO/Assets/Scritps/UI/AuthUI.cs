﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Globals;

[System.Serializable]
public struct LoginData
{
    [SerializeField] private TMP_InputField email;
    [SerializeField] private TMP_InputField password;
    public string Email => email.text;
    public string Password => password.text;

}

[System.Serializable]
public struct RegisterData
{
    [SerializeField] private TMP_InputField username;
    [SerializeField] private TMP_InputField email;
    [SerializeField] private TMP_InputField password;
    [SerializeField] private TMP_InputField verifyPassword;

    public string UserName => username.text;
    public string Email => email.text;
    public string Password => password.text;
    public string VerifyPassword => verifyPassword.text;
}

public class AuthUI : MonoBehaviour
{

    [SerializeField] private Button loginButton;
    [SerializeField] private Button registerButton;

    [SerializeField] private Button goToRegisterButton;
    [SerializeField] private Button goToLoginButton;

    [SerializeField] private LoginData loginData;
    [SerializeField] private RegisterData registerData;

    private void Awake()
    {
        loginButton.onClick.AddListener
        (
            () => StartCoroutine(AuthManager.Login(loginData.Email, loginData.Password))
        );

        registerButton.onClick.AddListener
        (
            () => StartCoroutine(AuthManager.Register(registerData.UserName, registerData.Email, registerData.Password, registerData.VerifyPassword))
        );

        goToRegisterButton.onClick.AddListener
        (
            () => ScreensManager.ShowScreen(ScreenName.RegisterScreen)
        );

        goToLoginButton.onClick.AddListener
        (
            () => ScreensManager.ShowScreen(ScreenName.LoginScreen)
        );
    }
}
