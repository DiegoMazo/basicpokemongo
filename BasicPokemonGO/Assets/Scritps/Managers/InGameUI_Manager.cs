﻿using System.Collections;
using UnityEngine;
using TMPro;
using Globals;

public class InGameUI_Manager : MonoBehaviour
{
    public static InGameUI_Manager Instance { get; private set; }
    [SerializeField] private TextMeshProUGUI inGameText;
    private const float WAIT_TIME = 5f;
    public void SetInGameText(string value = "") => StartCoroutine(SetText(value));


    private IEnumerator SetText(string value)
    {
        if (string.IsNullOrEmpty(value)) yield break;

        inGameText.text = value;
        yield return new WaitForSeconds(WAIT_TIME);
        inGameText.text = string.Empty;
    }


    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        GlobalEvents.Instance.OnCatchFailed.AddListener(() => SetInGameText(InGameMsgs.fail));
        GlobalEvents.Instance.OnPokemonScape.AddListener(() => SetInGameText(InGameMsgs.HasScaped));
    }
}
