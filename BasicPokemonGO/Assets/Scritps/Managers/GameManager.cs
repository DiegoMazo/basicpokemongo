﻿using System.Collections;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    private const short MIN_PROV = -5, MAX_PROV = 5;
    private const float RESULT_TIME = 3.5f;
    private const float TIME_TO_CLOSE_APP = 5f;
    public static GameManager Instance { get; private set; }
    private IEnumerator C_DecideResult()
    {
        yield return new WaitForSeconds(RESULT_TIME);
        if (Random.Range(MIN_PROV, MAX_PROV) > ushort.MinValue)
            GlobalEvents.CatchSuccess();
        else

            GlobalEvents.PokemonScape();
    }

    public void DecideResult() => StartCoroutine(C_DecideResult());

    private void CloseApp () => Application.Quit();

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);

        GlobalEvents.Instance.OnGameEnded.AddListener(() => Invoke("CloseApp", TIME_TO_CLOSE_APP));
    }
}
