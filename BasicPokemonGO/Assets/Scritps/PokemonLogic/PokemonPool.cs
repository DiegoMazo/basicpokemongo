﻿using UnityEngine;

public class PokemonPool : BasicPoolSystem
{
    public static PokemonPool Instance { get; private set; }
    [SerializeField] private Pokemon[] pokemons;
    public override IPooleable GetPooledObject()
    {

        if (Instance.pooledObjects.Count == ushort.MinValue) return null;

        Pokemon pokemon = (Pokemon)Instance.pooledObjects[Random.Range(ushort.MinValue, Instance.pooledObjects.Count)];
        pokemon.gameObject.SetActive(true);
        return pokemon;
    }

    public void RemovePokemon(Pokemon pokemon) => pooledObjects.Remove(pokemon);

    private void Awake()
    {
        if (!Instance) Instance = this;
        else if (Instance != this) Destroy(gameObject);
    }



    private void Start()
    {
        base.SetObjectsToPool(pokemons);
    }
}
