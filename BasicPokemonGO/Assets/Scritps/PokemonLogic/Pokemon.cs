﻿using UnityEngine;
using Globals;

public class Pokemon : MonoBehaviour, IPooleable
{
    [SerializeField] private int id;
    [SerializeField] private Transform captureHelperTarget;

    public Vector3 CaptureTargetPosition => captureHelperTarget.position;
    public int ID => id;
    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void ShowAt(Vector3 position, Quaternion rotation)
    {
        transform.SetPositionAndRotation(position ,rotation);
    }

    public void Scape() 
    {
        Hide();
        InGameUI_Manager.Instance.SetInGameText(InGameMsgs.HasScaped);
    }
}
