﻿using System.Collections;
using UnityEngine;

public class Pokeball : MonoBehaviour
{
    private const float FAIL_OFFSET = 0.1f;

    [SerializeField] private AnimationCurve curve;
    [SerializeField] private Transform initialPos;
    private Vector3 start, end;

    private float time;

    private bool fail;
    private bool cantThrow;

    private void Trhow()
    {
        time += Time.deltaTime;
        Vector3 pos = Vector3.Lerp(start, !fail ? end : end + Vector3.right * FAIL_OFFSET, time);
        pos.y += curve.Evaluate(time);
        transform.position = pos;

        if (Vector3.Distance(transform.position, end) < 0.15f)
        {
            Restart();
            if (fail)
                GlobalEvents.CatchFailed();
            else
            {
                PokemonSpawner.SpawnedPokemon.Hide();
                GameManager.Instance.DecideResult();
            }
        }
    }
    public void SetTrhow(Vector3 start, Vector3 end, bool fail)
    {
        cantThrow = true;
        this.start = start;
        this.end = end;
        this.fail = fail;
    }

    public void Restart()
    {
        transform.position = initialPos.position;
        cantThrow = false;
        gameObject.SetActive(!cantThrow);
        time = ushort.MinValue;
    }
    private void Start()
    {
        cantThrow = false;
    }
    private void Update()
    {
        if (cantThrow) Trhow();
    }
}