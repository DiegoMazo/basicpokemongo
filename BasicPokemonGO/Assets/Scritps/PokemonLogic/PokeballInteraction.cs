﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class PokeballInteraction : MonoBehaviour
{
    private const float MIN_INDICATOR_SCARLE = 0.015f;
    [SerializeField] private Pokeball pokeBall;
    [SerializeField] private CaptureHelperHandler captureHelper;
    private bool thrownOff = false;
    private bool canInteract = false;

    private bool FingerOverUI()
    {
        var eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, results);
        return results.Count(x => x.gameObject.GetComponent<RectTransform>()) > 0;
    }

    private void TouchInteraction()
    {
        if (Input.touchCount > ushort.MinValue && !thrownOff)
        {
            Touch touch = Input.GetTouch(ushort.MinValue);

            if (EventSystem.current.IsPointerOverGameObject(touch.fingerId) || FingerOverUI()) return;

            switch (touch.phase)
            {
                case TouchPhase.Began:

                    captureHelper.SetPos(PokemonSpawner.SpawnedPokemon.CaptureTargetPosition);
                    captureHelper.ShowIndicator();
                    break;
                case TouchPhase.Ended:
                case TouchPhase.Canceled:
                    bool fail = captureHelper.GetCurrentScale < MIN_INDICATOR_SCARLE;
                    pokeBall.SetTrhow(pokeBall.transform.position, PokemonSpawner.SpawnedPokemon.CaptureTargetPosition, fail);
                    thrownOff = true;
                    captureHelper.HideIndicator();
                    break;
            }
        }
    }

    private void Awake()
    {
        GlobalEvents.Instance.OnCatchFailed.AddListener
        (
             () =>
             {
                 thrownOff = false;
             }
         );

        GlobalEvents.Instance.OnPokemonScape.AddListener
        (
           () =>
           {
               thrownOff = false;
               canInteract = true;
           }
        );

        GlobalEvents.Instance.OnCatchSuccess.AddListener
        (
            () =>
            {
                thrownOff = false;
                InGameUI_Manager.Instance.SetInGameText(Globals.InGameMsgs.succes);
   
            }
        );

        GlobalEvents.Instance.OnPokemonDetected.AddListener
        (
            () =>
            {
                thrownOff = false;
                canInteract = true;
            }
        );
    }
    void Update()
    {
        if (canInteract)
            TouchInteraction();
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(100, 100, 250, 100), FingerOverUI().ToString());
    }
}
