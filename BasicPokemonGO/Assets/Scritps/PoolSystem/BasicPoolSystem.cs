﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BasicPoolSystem : MonoBehaviour
{
    protected IPooleable[] objectsToPool;
    [SerializeField]protected List<IPooleable> pooledObjects = new List<IPooleable>();

    protected void SetObjectsToPool(IPooleable[] objectsToPool) 
    {
        this.objectsToPool = objectsToPool;
        CreatePool();
    } 
    private void CreatePool()
    {
        IPooleable tmp;
        for (int i = 0; i < objectsToPool.Length; i++)
        {
            tmp = Instantiate((Object)objectsToPool[i]) as IPooleable;
            tmp.Hide();
            pooledObjects.Add(tmp);
        }
    }
    public abstract IPooleable GetPooledObject();
}
