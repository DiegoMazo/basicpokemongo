﻿using UnityEngine;

public interface IPooleable 
{
    void ShowAt(Vector3 position, Quaternion rotation);
    void Hide();
}
