﻿using Globals;
using System.Collections;

using UnityEngine.Networking;
using UnityEngine;

public class GetPokemonData: MonoBehaviour
{
    public static GetPokemonData Instance { get; private set; }
    private IEnumerator GetTexture(int id, CardData card)
    {
        string url = string.Format(API_Rest.pokemonImageurl, id);
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture2D txt2D = ((DownloadHandlerTexture)www.downloadHandler).texture;
            card.SetImage(txt2D);
        }
    }
    private IEnumerator GetData(int id, CardData card)
    {
        string url = string.Format(API_Rest.pokemonGeneralData, id);
        UnityWebRequest request = UnityWebRequest.Get(url);

        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            card.SetData(JsonUtility.FromJson<PokemonGeneralData>(request.downloadHandler.text));
        }
    }

    public static void GetAllData(int id, CardData card)
    {
        Instance.StartCoroutine(Instance.GetData(id + 1, card));
        Instance.StartCoroutine(Instance.GetTexture(id + 1, card));
    }

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }
}
