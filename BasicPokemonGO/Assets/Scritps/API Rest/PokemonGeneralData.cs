﻿[System.Serializable]
public class PokemonGeneralData 
{
    public int id;
    public string name;
    public string weight;
    public string height;
    public UnityEngine.Texture2D image;
}
