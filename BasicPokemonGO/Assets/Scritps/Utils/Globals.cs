﻿namespace Globals
{
    public struct ScreenName
    {
        public const string LoginScreen = "LoginScreen";
        public const string RegisterScreen = "RegisterScreen";
    }
    public struct API_Rest
    {
        public const string pokemonGeneralData = "https://pokeapi.co/api/v2/pokemon/{0}";
        public const string pokemonImageurl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/{0}.png";
    }

    public struct InGameMsgs
    {
        public const string fail = "you failed, try again";
        public const string succes = "Well done, you caught it";
        public const string HasScaped = "Has escaped";
        public const string Lookaround = "Look around and look for a pokemon";
        public const string APokemonHasAppeared = "A Pokemon has appeared Catch it!";
        public const string AllPokemonCaptured = "You have captured all the pokemon!";
    }
}

namespace Globals.Auth 
{
    public struct AuthMsg 
    {
        public const string UserSignedInSuccessfully = "User signed in successfully";
        public const string RegisterSuccessfully = "Register successfully";

        public const string EmptyUserName = "Missing Username";
        public const string PasswordDoesNotMatch = "Password Does Not Match!";
        public const string FailedToRegister = "Failed to register task with {0}";
        public const string FailedToLogin = "Failed to login task with {0}";

        public const string RegisterFailed = "Register Failed!";

        public const string MissingEmail = "Missing Email";

        public const string MissingPassword = "Missing Password";
        public const string WeakPassword = "Weak Password";
        public const string WrongPassword = "Wrong Password";
        public const string EmailAlreadyInUse= "Email Already In Use";
        public const string InvalidEmail = "Invalid Email";
        public const string UserNotFound = "User Not Found";
        public const string UsernameSetFailed = "Username Set Failed!";
    }
}

