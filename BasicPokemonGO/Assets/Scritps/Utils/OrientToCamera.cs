﻿using UnityEngine;

public class OrientToCamera : MonoBehaviour
{
    [SerializeField] private Camera targetCamera;
    private void Update()
    {
        transform.LookAt(targetCamera.transform);
    }
}
