﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GlobalEvents : MonoBehaviour
{
    public static GlobalEvents Instance { get; private set; }
    [HideInInspector] public UnityEvent OnPokemonDetected;
    [HideInInspector] public UnityEvent OnCatchFailed;
    [HideInInspector] public UnityEvent OnCatchSuccess;
    [HideInInspector] public UnityEvent OnPokemonScape;
    [HideInInspector] public UnityEvent OnGameEnded;

    public static void PokemonDetected() => Instance.OnPokemonDetected?.Invoke();
    public static void CatchFailed() => Instance.OnCatchFailed?.Invoke();
    public static void CatchSuccess() => Instance.OnCatchSuccess?.Invoke();
    public static void PokemonScape() => Instance.OnPokemonScape?.Invoke();
    public static void GameEnded () => Instance.OnGameEnded?.Invoke();

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else if (Instance != this)
            Destroy(gameObject);
    }
}
